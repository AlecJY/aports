# Contributor: Patrycja Rosa <alpine@ptrcnull.me>
# Maintainer: Patrycja Rosa <alpine@ptrcnull.me>
pkgname=fcitx5
pkgver=5.0.16
pkgrel=0
pkgdesc="Generic input method framework"
url="https://github.com/fcitx/fcitx5"
# armhf: blocked by extra-cmake-modules, xcb-imdkit-dev
arch="all !armhf"
license="LGPL-2.1-or-later"
makedepends="
	cairo-dev
	dbus-dev
	enchant2-dev
	extra-cmake-modules
	fmt-dev
	gdk-pixbuf-dev
	iso-codes
	iso-codes-dev
	iso-codes-lang
	json-c-dev
	libevent-dev
	libxkbcommon-dev
	libxkbfile-dev
	mesa-dev
	pango-dev
	samurai
	wayland-dev
	wayland-protocols
	xcb-imdkit-dev
	xcb-util-keysyms-dev
	xcb-util-wm-dev
	"
subpackages="$pkgname-lang $pkgname-dev"
source="https://github.com/fcitx/fcitx5/archive/$pkgver/fcitx5-$pkgver.tar.gz"
options="!check" # requires working dbus

build() {
	if [ "$CBUILD" != "$CHOST" ]; then
		CMAKE_CROSSOPTS="-DCMAKE_SYSTEM_NAME=Linux -DCMAKE_HOST_SYSTEM_NAME=Linux"
	fi
	cmake -G Ninja -B build \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib \
		-DBUILD_SHARED_LIBS=True \
		-DCMAKE_BUILD_TYPE=Release \
		$CMAKE_CROSSOPTS .
	cmake --build build
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}

sha512sums="
cf27e414da54618d4d914b3acd291a90cfa7f806e78ef902860228f1ce0e689fac57dee46d335a1ce5fcffa8d31b55f745fd14015ece9a0311d5aba868fa9735  fcitx5-5.0.16.tar.gz
"
