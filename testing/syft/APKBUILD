# Contributor: Michał Polański <michal@polanski.me>
# Maintainer: Michał Polański <michal@polanski.me>
pkgname=syft
pkgver=0.46.3
pkgrel=0
pkgdesc="Generate a Software Bill of Materials (SBOM) from container images and filesystems"
url="https://github.com/anchore/syft"
license="Apache-2.0"
arch="all !armhf !armv7 !x86" # FTBFS on 32-bit arches
makedepends="go"
source="https://github.com/anchore/syft/archive/v$pkgver/syft-$pkgver.tar.gz"
options="!check" # tests need docker

export GOFLAGS="$GOFLAGS -trimpath -mod=readonly -modcacherw"
export GOPATH="$srcdir"
export CGO_ENABLED=0

build() {
	go build -ldflags "-s -w
		-X github.com/anchore/syft/internal/version.version=$pkgver
		" \
		-o bin/syft ./cmd/syft
}

check() {
	go test ./...
}

package() {
	install -Dm755 bin/syft "$pkgdir"/usr/bin/syft
}

sha512sums="
3a690e7ec15bf721f4e4011eec09e834526b4e82b0cfe46cf0abe4c39f5fc4ce1a7876faa92ea95fbfb3f280ff615d1837fc9903ec76792df4e6e5250a47b1d4  syft-0.46.3.tar.gz
"
