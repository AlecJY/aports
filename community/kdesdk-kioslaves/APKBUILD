# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: Bart Ribbers <bribbers@disroot.org>
pkgname=kdesdk-kioslaves
pkgver=22.04.1
pkgrel=0
# armhf blocked by extra-cmake-modules
# s390x and riscv64 blocked by polkit -> kio
arch="all !armhf !s390x !riscv64"
url="https://kde.org/applications/development"
pkgdesc="KIO-Slaves"
license="GPL-2.0-or-later"
makedepends="
	extra-cmake-modules
	ki18n-dev
	kio-dev
	perl-dev
	qt5-qtbase-dev
	samurai
	"
source="https://download.kde.org/stable/release-service/$pkgver/src/kdesdk-kioslaves-$pkgver.tar.xz"
subpackages="$pkgname-lang"
options="!check" # No tests

build() {
	cmake -B build -G Ninja \
		-DCMAKE_BUILD_TYPE=RelWithDebInfo \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib
	cmake --build build
}

check() {
	cd build
	CTEST_OUTPUT_ON_FAILURE=TRUE ctest
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}

sha512sums="
212efc14e58ca43fb2685112d0fea6ce086456646ca14df692877309505ca7d4958def3d9e519eae57c3ec6beb24846297a7759f62c4fa0d3102801859e9d079  kdesdk-kioslaves-22.04.1.tar.xz
"
