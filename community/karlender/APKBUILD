# Contributor: Newbyte <newbie13xd@gmail.com>
# Maintainer: Newbyte <newbie13xd@gmail.com>
pkgname=karlender
pkgver=0.4.3
pkgrel=0
pkgdesc="Mobile friendly GTK based calendar app written in Rust"
url="https://gitlab.com/loers/karlender"
# ppc64le: build script for dependency ring 0.16.20 fails
# s390x, riscv64: blocked by rust/cargo
arch="all !ppc64le !riscv64 !s390x"
license="GPL-3.0-or-later"
makedepends="
	cargo
	cargo-gra
	libadwaita-dev
	"
source="https://gitlab.com/loers/karlender/-/archive/v$pkgver/karlender-v$pkgver.tar.gz
	13.patch
	"
options="!check"
builddir="$srcdir/$pkgname-v$pkgver"

export CARGO_PROFILE_RELEASE_CODEGEN_UNITS=1
export CARGO_PROFILE_RELEASE_LTO="true"
export CARGO_PROFILE_RELEASE_OPT_LEVEL="s"
export CARGO_PROFILE_RELEASE_PANIC="abort"

prepare() {
	default_prepare

	cargo fetch --locked
}

build() {
	cargo gra gen
	cargo build --release --frozen
}

package() {
	cargo install --locked --root "$pkgdir/usr" --path .
	make -C target/gra-gen install ROOT="$pkgdir/usr"

	rm "$pkgdir"/usr/.crates.toml
	rm "$pkgdir"/usr/.crates2.json
}

sha512sums="
522d55132eb9e9e2135f1c7dd08c2bd0169053ba13e4eb35177177f5d57a1ea9c05b9f4b53e8a176a7184aca44ab21d42d99106abbf2ec9a05f30191cb002daa  karlender-v0.4.3.tar.gz
0ace77b025e1d95e6bd59be1773d2334571e43454fde8012ba5ff609c5c2722b9ffff10a602a65799388f1f906c02dc90fe7b027c3fcfc01294229e772d40246  13.patch
"
