# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: Bart Ribbers <bribbers@disroot.org>
pkgname=kiten
pkgver=22.04.1
pkgrel=0
# armhf blocked by extra-cmake-modules
# s390x and riscv64 blocked by polkit -> kconfigwidgets
arch="all !armhf !s390x !riscv64"
url="https://edu.kde.org/kiten/"
pkgdesc="Japanese Reference/Study Tool"
license="GPL-2.0-or-later AND GFDL-1.2-only"
makedepends="
	extra-cmake-modules
	karchive-dev
	kcompletion-dev
	kconfig-dev
	kconfigwidgets-dev
	kcoreaddons-dev
	kcrash-dev
	kdoctools-dev
	khtml-dev
	ki18n-dev
	knotifications-dev
	kxmlgui-dev
	qt5-qtbase-dev
	samurai
	"
source="https://download.kde.org/stable/release-service/$pkgver/src/kiten-$pkgver.tar.xz"
subpackages="$pkgname-doc $pkgname-lang $pkgname-dev"
options="!check" # No tests

build() {
	cmake -B build -G Ninja \
		-DCMAKE_BUILD_TYPE=RelWithDebInfo \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib
	cmake --build build
}

check() {
	cd build
	CTEST_OUTPUT_ON_FAILURE=TRUE ctest
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}

sha512sums="
e3e584eda3ba4c66e90f45857ac2d05ca197a7611c569b848ed11d22dbead8a82e37c381e76bdc0a69bd42fe35b3a6187d9d8c671a599fc4639e3ca3260e55c8  kiten-22.04.1.tar.xz
"
