# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: Bart Ribbers <bribbers@disroot.org>
pkgname=libkipi
pkgver=22.04.1
pkgrel=0
pkgdesc="KDE Image Plugin Interface library"
# armhf blocked by extra-cmake-modules
# s390x and riscv64 blocked by kxmlgui
arch="all !armhf !s390x !riscv64"
url="https://www.digikam.org/"
license="GPL-2.0-or-later AND LGPL-2.0-or-later"
depends_dev="
	kconfig-dev
	kservice-dev
	kxmlgui-dev
	qt5-qtbase-dev
	"
makedepends="$depends_dev
	extra-cmake-modules
	libjpeg-turbo-dev
	libkexiv2-dev
	samurai
	tiff-dev
	"
source="https://download.kde.org/stable/release-service/$pkgver/src/libkipi-$pkgver.tar.xz"
subpackages="$pkgname-dev"
options="!check" # No tests

build() {
	cmake -B build -G Ninja \
		-DCMAKE_BUILD_TYPE=RelWithDebInfo \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib
	cmake --build build
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}

sha512sums="
0bab4c3a59dd0b731738cb84b3635b0ec00339419a1376759f87d730e49540c7fa9096130da979c92c8d8183fb1415998907f9935c06a8c85ce27e2a8b94367f  libkipi-22.04.1.tar.xz
"
