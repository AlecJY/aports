# Contributor: Thomas Boerger <thomas@webhippie.de>
# Contributor: Gennady Feldman <gena01@gmail.com>
# Contributor: Sergii Sadovyi <serg.sadovoi@gmail.com>
# Contributor: Galen Abell <galen@galenabell.com>
# Maintainer: Thomas Boerger <thomas@webhippie.de>
pkgname=terraform
pkgver=1.2.1
pkgrel=0
pkgdesc="Building, changing and combining infrastructure safely and efficiently"
url="https://www.terraform.io/"
arch="all"
license="MPL-2.0"
makedepends="go"
checkdepends="openssh-client"
source="$pkgname-$pkgver.tar.gz::https://github.com/hashicorp/terraform/archive/refs/tags/v$pkgver.tar.gz"
options="net !check" # extremely flaky

export GOFLAGS="$GOFLAGS -modcacherw -mod=readonly -trimpath"
export GOPATH="$srcdir"

build() {
	go build -v -o bin/$pkgname \
		-ldflags "-X main.GitCommit=v$pkgver -X github.com/hashicorp/terraform/version.Prerelease="
}

check() {
	case "$CARCH" in
	arm*|x86)
		go list . | xargs -t -n4 \
			go test -timeout=2m -parallel=4
		;;
	*)
		go test ./...
		;;
	esac
	bin/$pkgname -v
}

package() {
	install -Dm755 "$builddir"/bin/$pkgname -t "$pkgdir"/usr/bin
}

sha512sums="
6a7af43744f928722f71d8ffb3401d594b250fad03df79ae403acbdd1887365de868256d47bdde42a170a4c01d76f789f52de821267db666407e42776deb0b25  terraform-1.2.1.tar.gz
"
