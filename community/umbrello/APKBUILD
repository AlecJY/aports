# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: Bart Ribbers <bribbers@disroot.org>
pkgname=umbrello
pkgver=22.04.1
pkgrel=0
arch="all !armhf !s390x !riscv64" # Blocked by extra-cmake-modules and rust
url="https://umbrello.kde.org/"
pkgdesc="GUI for diagramming Unified Modelling Language (UML)"
license="GPL-2.0-or-later AND LGPL-2.0-or-later AND GFDL-1.2-only"
makedepends="
	extra-cmake-modules
	karchive-dev
	kcompletion-dev
	kconfig-dev
	kcoreaddons-dev
	kcrash-dev
	kdelibs4support-dev
	kdoctools-dev
	ki18n-dev
	kiconthemes-dev
	kio-dev
	kitemmodels-dev
	ktexteditor-dev
	kwidgetsaddons-dev
	kwindowsystem-dev
	kxmlgui-dev
	qt5-qtbase-dev
	samurai
	"
checkdepends="xvfb-run"
source="https://download.kde.org/stable/release-service/$pkgver/src/umbrello-$pkgver.tar.xz"
subpackages="$pkgname-doc $pkgname-lang"

case "$CARCH" in
	ppc64le|armv7) options="!check";; # FIXME: testsuite fails
esac

build() {
	cmake -B build -G Ninja \
		-DCMAKE_BUILD_TYPE=RelWithDebInfo \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib \
		-DBUILD_KF5=ON
	cmake --build build
}

check() {
	cd build
	CTEST_OUTPUT_ON_FAILURE=TRUE xvfb-run ctest
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}

sha512sums="
c58051d1e0c8bcedb824e2c582292452fa27743446da7666690ddbbdf5be80c412a43ee50168fb09b6a6311bf7c8d09e62888c149ba400636202b50ab1ec45ea  umbrello-22.04.1.tar.xz
"
